﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VendasApi.Domain.Models;
using VendasApi.Domain.Services;
using VendaWebApi.Dtos;

namespace VendaWebApi.Controllers
{

    [Route("api/[controller]")]
    [ApiVersion("1.0")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService vendaService;
        private readonly IMapper mapper;

        public VendaController(IVendaService vendaService, IMapper mapper)
        {
            this.vendaService = vendaService ??
                throw new ArgumentNullException(nameof(vendaService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Busca venda realizada.
        /// </summary>
        /// <param name="id">
        ///     Criterios de pesquisa para venda.
        /// </param>
        /// <response code="200">Retorno da venda encontrada com base no critério ID.</response>
        /// <response code="204">Não encontrada venda com o Id informado.</response>
        /// </response>
        [Microsoft.AspNetCore.Mvc.HttpGet("Buscar")]
        public async Task<IActionResult> BuscarVendaAsync(
             long id)
        {
            var venda = await vendaService.BuscarVendaAsync(id);

            if (venda != null)
            {
                return Ok(venda);
            }

            return NoContent();
        }

        /// <summary>
        /// Atualizar venda.
        /// </summary>
        /// <param name="atualizarVendaPost">
        ///     Criterios de pesquisa de venda a ser atualizada.
        /// </param>
        /// <response code="200">Atualizacao executada com Sucesso.</response>
        /// <response code="204">Objeto não encontrado , Atualizacao não executada.</response>
        /// </response>
        [Microsoft.AspNetCore.Mvc.HttpPost("Atualizar")]
        public async Task<IActionResult> AtualizarVendaAsync(
              [FromQuery]AtualizarVendaPost atualizarVendaPost)
        {

            var resultado = await vendaService.AtualizarVendaAsync(atualizarVendaPost.Id, atualizarVendaPost.Status);

            if (resultado)
            {
                return Ok();
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// Registrar venda.
        /// </summary>
        /// <param name="venda">
        ///    Informações que compoe a venda a ser registrada.
        /// </param>
        /// <response code="200">Registro de venda executado com sucesso.</response>
        /// <response code="500">Erro Interno - Registro de venda não executado.</response>
        [Microsoft.AspNetCore.Mvc.HttpPost("Registrar")]
        public async Task<IActionResult> RegistrarVendaAsync(
              VendaPost vendaPost)
        {
            Venda venda = mapper.Map<Venda>(vendaPost);

            var resultado = await vendaService.RegistrarVendaAsync(venda);

            if (resultado)
            {
                return Ok(resultado);
            }
            else
            {
                return StatusCode(500);
            }

        }
    }
}
