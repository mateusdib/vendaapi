﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendasApi.Domain.Models;

namespace VendaWebApi.Dtos
{
    public class AtualizarVendaPost
    {
        /// <summary>
        /// Identificador da venda a ser pesquisado.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Status da venda a ser atualizado.
        /// </summary>
        public Status Status { get; set; }
    }
}
