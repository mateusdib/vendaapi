﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendaWebApi.Dtos
{
    public class ProdutoPost
    {
        /// <summary>
        /// Codigo do produto.
        /// </summary>
        public long Codigo { get; set; }

        /// <summary>
        /// Nome do produto.
        /// </summary>
        public string Nome { get; set; }


        /// <summary>
        /// Valor do produto.
        /// </summary>
        public double Valor { get; set; }
    }
}
