﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendasApi.Domain.Models;

namespace VendaWebApi.Dtos
{
    public class VendaPost
    {
        /// <summary>
        /// Identificador da venda.
        /// </summary>
        public VendedorPost Vendedor { get; set; }

        /// <summary>
        /// Produtos que compoe a venda.
        /// </summary>
        public IEnumerable<ProdutoPost> Produto { get; set; }

        /// <summary>
        /// Status da venda.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Data que foi realizada a venda.
        /// </summary>
        public DateTimeOffset DataVenda { get; set; }
    }
}
