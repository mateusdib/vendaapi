﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendaWebApi.Dtos
{
    public class VendedorPost
    {
        /// <summary>
        /// Codigo do vendedor.
        /// </summary>
        public long Codigo { get; set; }

        /// <summary>
        /// Nome do vendedor.
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Cpf Vendedor.
        /// </summary>
        public long Cpf { get; set; }

        /// <summary>
        /// Cpf Vendedor.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefone de contato do Vendedor.
        /// </summary>
        public string Telefone { get; set; }
    }
}
