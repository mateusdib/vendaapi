using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using VendasApi.Adapter;
using VendasApi.Application;
using VendasApi.Domain.Models;
using VendasApi.Domain.Services;
using VendasApiAdapter;
using VendaWebApi.Dtos;

namespace VendaWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase(databaseName: "Test"),
                ServiceLifetime.Singleton, ServiceLifetime.Singleton);

            services.AddControllers();

            services.AddScoped<IVendaService, VendaService>();
            services.AddScoped<IVendaAdapter, VendaAdapter>();

            services.AddSwaggerGen(c => {

                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Venda Produtos Api",
                        Version = "v1",
                        Description = "Exemplo de API REST criada com o NET Core 3.1" +
                        "Arquitetura utilizada: Hexagonal (by Alistair Cockburn). " +
                        "Tecnologias utilizadas: Swagger,AutoMapper,Teste Unitarios xUNIT,EF CORE in memory,IOC",
                        Contact = new OpenApiContact
                        {
                            Name = "Matheus Dias",
                            Url = new Uri("https://gitlab.com/mateusdib")
                        }
                    });

               
               

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "VendaApi");
                c.RoutePrefix = string.Empty;
            });


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
