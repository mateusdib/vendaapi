﻿using AutoMapper;
using System.Collections.Generic;
using VendasApi.Domain.Models;
using VendaWebApi.Dtos;

namespace VendaWebApi
{
    public class WebApiMapperProfile : Profile
    {
        public WebApiMapperProfile()
        {
            CreateMap<VendaPost,Venda>();
            CreateMap<VendedorPost, Vendedor>();
            CreateMap<ProdutoPost, Produto>();




        }
    }
}
