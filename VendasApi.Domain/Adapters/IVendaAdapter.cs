﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VendasApi.Domain.Models;

namespace VendasApi.Domain.Services
{
   public interface IVendaAdapter
    {
        /// <summary>
        /// Realiza busca de venda.
        /// Este adaptador consiste em um exemplo para buscar um mock contendo dados da venda
        /// </summary>
        /// <param name="Id">Indentificador para pesquisa.</param>
        /// <returns>
        ///     Venda encontradas conforme indentificador informado.
        /// </returns>
        Task<Venda> BuscarVendaAsync(long Id);

        /// <summary>
        /// Realiza busca de venda.
        /// Este adaptador consiste em um exemplo para atualizar em um mock contendo dados da venda
        /// </summary>
        /// <param name="Venda">Venda para ser atualizada.</param>
        /// <returns>
        ///       Retorna booleano informando atualizaçao da venda
        /// </returns>
        Task<bool> AtualizarVendaAsync(Venda Venda);

        /// <summary>
        /// Realiza registro de venda.
        /// Este adaptador consiste em um exemplo para registrar em um mock informações de venda
        /// </summary>
        /// <param name="Venda">Indentificador para pesquisa.</param>
        /// <returns>
        ///     Retorna booleano validando registro da venda
        /// </returns>
        Task<bool> RegistrarVendaAsync(Venda Venda);

    }
}
