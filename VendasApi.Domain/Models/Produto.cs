﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendasApi.Domain.Models
{
    public class Produto
    {
        /// <summary>
        /// Id do produto.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Codigo do produto.
        /// </summary>
        public long Codigo { get; set; }

        /// <summary>
        /// Nome do produto.
        /// </summary>
        public string Nome { get; set; }


        /// <summary>
        /// Valor do produto.
        /// </summary>
        public double Valor { get; set; }
    }
}
