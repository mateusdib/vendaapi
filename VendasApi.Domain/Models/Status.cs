﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendasApi.Domain.Models
{
    /// <summary>
    /// 0 - AguardandoPagamento -Aguardando Pagamento.
    /// 1 - PagamentoAprovado - Pagamento foi Aprovado.
    /// 2 - EnviadoTransportadora - Enviado para a Transportadora.
    /// 3 - Entregue - Produto foi entregue.
    /// 4 - Cancelada - Venda foi Cancelada.
    /// </summary>
    public enum Status
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoTransportadora,
        Entregue,
        Cancelada
    };

}
