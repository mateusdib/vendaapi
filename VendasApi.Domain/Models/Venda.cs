﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendasApi.Domain.Models
{
    public class Venda
    {
        /// <summary>
        /// Identificador da venda.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Identificador da venda.
        /// </summary>
        public Vendedor Vendedor { get; set; }

        /// <summary>
        /// Produtos que compoe a venda.
        /// </summary>
        public IEnumerable<Produto> Produto { get; set; }

        /// <summary>
        /// Status da venda.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Data que foi realizada a venda.
        /// </summary>
        public DateTimeOffset DataVenda { get; set; }

    }
}
