﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendasApi.Domain.Models
{
    public class Vendedor
    {
        /// <summary>
        /// Codigo do vendedor.
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Codigo do vendedor.
        /// </summary>
        public long Codigo { get; set; }
        
        /// <summary>
        /// Nome do vendedor.
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Cpf Vendedor.
        /// </summary>
        public long Cpf { get; set; }

        /// <summary>
        /// Cpf Vendedor.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefone de contato do Vendedor.
        /// </summary>
        public string Telefone { get; set; }


    }
}
