﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VendasApi.Domain.Models;

namespace VendasApi.Domain.Services
{
    public interface IVendaService
    {
        /// <summary>
        /// Realiza busca de venda.
        /// Este adaptador consiste em um exemplo para buscar um mock contendo dados da venda
        /// </summary>
        /// <param name="Id">Indentificador para pesquisa.</param>
        /// <returns>
        ///     Venda encontradas conforme indentificador informado.
        /// </returns>
        Task<Venda> BuscarVendaAsync(long id);

        /// <summary>
        /// Realiza busca de venda.
        /// Este adaptador consiste em um exemplo para atualizar em um mock contendo dados da venda
        /// </summary>
        /// <param name="Id">Identificador da venda.
        /// <param name="statusVenda">Status da venda 
        /// para atualizacao.</param>
        /// <returns>
        ///     Venda encontradas conforme indentificador informado.
        /// </returns>
        Task<bool> AtualizarVendaAsync(long Id, Status statusVenda);

        /// <summary>
        /// Realiza registro de venda.
        /// Este adaptador consiste em um exemplo para registrar em um mock informações de venda
        /// </summary>
        /// <param name="venda">venda contendo dados para registro.</param>
        /// <returns>
        ///     Retorna booleano validando registro da venda
        /// </returns>
        Task<bool> RegistrarVendaAsync(Venda venda);
    }
}
