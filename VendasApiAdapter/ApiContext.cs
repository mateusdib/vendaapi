﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VendasApi.Domain.Models;

namespace VendasApi.Adapter
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
          : base(options)
        { }
        public DbSet<Venda> Venda { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Produto>().HasNoKey();

            //modelBuilder.Entity<Vendedor>().HasNoKey();
           
            modelBuilder.Entity<Vendedor>()
            .Property(f => f.Id)
            .ValueGeneratedOnAdd();

            modelBuilder.Entity<Produto>()
            .Property(f => f.Id)
            .ValueGeneratedOnAdd();

       
        }

    }
}
