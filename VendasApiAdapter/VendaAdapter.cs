﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using VendasApi.Adapter;
using VendasApi.Domain.Models;
using VendasApi.Domain.Services;

namespace VendasApiAdapter
{
    public class VendaAdapter : IVendaAdapter
    {
        private  ApiContext context;
       
        public VendaAdapter(ApiContext context)
        {
            this.context = context ??
               throw new ArgumentNullException(nameof(context));

            DbContextOptions<ApiContext>  options = new DbContextOptionsBuilder<ApiContext>()
                        .UseInMemoryDatabase(databaseName: "Test")
                        .Options;
        }
        public async Task<bool> AtualizarVendaAsync(Venda venda)
        {
            try
            {
                context.Update(venda);
                context.SaveChanges();

                return await context.Venda.AnyAsync();
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public async Task<Venda> BuscarVendaAsync(long id)
        {
            try
            {
                var venda = await Task.FromResult(context.Venda.Local.Where(x => x.Id == id).FirstOrDefault());

                return venda;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<bool> RegistrarVendaAsync(Venda venda)
        {
            try
            {
                 Venda vendaMock1 = new Venda
                    {
                        DataVenda = DateTimeOffset.Now,
                        Id = venda.Id,
                        Produto = venda.Produto,
                        Status = venda.Status,
                        Vendedor = venda.Vendedor,
                    };
                
                context.Venda.Add(vendaMock1);
                context.SaveChanges();

                return await context.Venda.AnyAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

       
    }
}
