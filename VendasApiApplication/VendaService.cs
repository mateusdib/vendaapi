﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VendasApi.Domain.Models;
using VendasApi.Domain.Services;

namespace VendasApi.Application
{

    public class VendaService : IVendaService
    {
        private readonly IVendaAdapter vendasAdapter;
        private readonly ILogger logger;


        public VendaService(IVendaAdapter vendasAdapter, ILoggerFactory loggerFactory)
        {
            this.vendasAdapter = vendasAdapter ??
                throw new ArgumentNullException(nameof(vendasAdapter));

            logger = loggerFactory?.CreateLogger<VendaService>() ??
                throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task<bool> AtualizarVendaAsync(long id, Status novoStatusVenda)
        {
            logger.LogInformation("Atualizando venda com os seguintes " +
                 "criterios de pesquisa: {@CriteriosPesquisa}",
                 new { Criterios = id });

            var result = false;

            var vendaCadastrada = await vendasAdapter.BuscarVendaAsync(id);

            if (vendaCadastrada != null)
            {
                if(ValidarStatusAtualizacao(vendaCadastrada.Status, novoStatusVenda))
                {
                    result = await vendasAdapter.AtualizarVendaAsync(vendaCadastrada);

                    logger.LogInformation("Atualizacao de venda concluida com sucesso.");
                }
                else
                {
                    logger.LogInformation("Criterios de status nao atendem a regra de negocio.");
                    result = false;
                }
            }

            return result;
        }

        public async Task<Venda> BuscarVendaAsync(long id)
        {
            
            logger.LogInformation("Buscando venda com os seguintes " +
               "criterios de pesquisa: {@CriteriosPesquisa}",
               new { Criterios = id });

            var resultado = await vendasAdapter.BuscarVendaAsync(id);

            logger.LogInformation("Dados da venda retornados " +
               "Venda: {@venda}",
               new { venda = resultado });

            return resultado;
        }

        public async Task<bool> RegistrarVendaAsync(Venda venda)
        {
            bool resultado;
            logger.LogInformation("Registrando venda com os seguintes " +
               "criterios de pesquisa: {@CriteriosPesquisa}",
               new { Criterios = venda });

            if (venda != null)
            {
                venda.Status = Status.AguardandoPagamento;
            }

            if(await vendasAdapter.RegistrarVendaAsync(venda))
            {
                logger.LogInformation("Registro de venda concluida com sucesso.");
                resultado = true;
            }
            else
            {
                logger.LogInformation("Erro ao incluir novo registro de venda.");
                resultado = false;
            }

            return resultado;
        }

        private bool ValidarStatusAtualizacao(Status statusProdutoExistente,Status novoStatus)
        {
            var result = false;

            if (statusProdutoExistente == Status.AguardandoPagamento)
            {
                if (novoStatus == Status.PagamentoAprovado)
                {
                    result = true;
                }
                else if (novoStatus == Status.Cancelada)
                {
                    result = true;
                }
            }
            else if (statusProdutoExistente == Status.PagamentoAprovado)
            {
                if (novoStatus == Status.EnviadoTransportadora)
                {
                    result = true;
                }
                else if (novoStatus == Status.Cancelada)
                {
                    result = true;
                }
            }
            else if (statusProdutoExistente == Status.EnviadoTransportadora)
            {
                if (novoStatus == Status.EnviadoTransportadora)
                {
                    result = true;
                }
            }
            return result;

        }
    }
}
